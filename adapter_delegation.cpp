// adapter.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream>
using namespace std;

// ��� ������������ ����� �������������� ������� ���������� �����
class FahrSensor
{
  public:
    double getFahrTemp() {																		// �������� ��������� ����������� � �������� ����������
      return temp_in_fahr;
    }
  private:
     static double temp_in_fahr;
};
double FahrSensor::temp_in_fahr = 212.0;

class Sensor
{    
  public:
    virtual ~Sensor() {}
    virtual double getTemperature() = 0;
};
  
class Adapter : public Sensor                                                                   //����������, ��� �������
{    
  public:
    Adapter( FahrSensor* p_fahrsensor ) : p_fsensor(p_fahrsensor) {
    }
   ~Adapter() {
      delete p_fsensor;
    }
    double fahrToCel(double fahr){
      return (fahr-32.0)*5.0/9.0;
    }
    double getTemperature() {
      return fahrToCel(p_fsensor->getFahrTemp());
    }
  private:
    FahrSensor* p_fsensor; 
};


int main()
{
  Sensor* my_adapter = new Adapter( new FahrSensor);
  cout << "Celsius temperature = " << my_adapter->getTemperature() << endl;
  delete p;    
  return 0;
}
