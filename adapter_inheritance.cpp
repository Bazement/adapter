// adapter.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream>
using namespace std;

// ��� ������������ ����� �������������� ������� ���������� �����
class FahrSensor
{
  public:
    double getFahrTemp() {																		// �������� ��������� ����������� � �������� ����������
      return temp_in_fahr;
    }
  protected:
     static double temp_in_fahr;
};
double FahrSensor::temp_in_fahr = 212.0;

class Sensor
{    
  public:
    virtual ~Sensor() {}
    virtual double getTemperature() = 0;
};
  
class Adapter : public Sensor, private FahrSensor                                                                  //����������, ��� �������
{    
  public:
    Adapter() {}
    double fahrToCel(double fahr){
      return (fahr-32.0)*5.0/9.0;
    }
    double getTemperature() {
      return fahrToCel(getFahrTemp());
    }
};


int main()
{
  Sensor* my_adapter = new Adapter();
  cout << "Celsius temperature = " << my_adapter->getTemperature() << endl;
  delete my_adapter;    
  return 0;
}
